#!/usr/bin/python 
#-*- coding: utf-8-*-
# exemple de echo server basic
# -----------------------------------------------------------------
# Escola del treball de Barcelona
# ASIX Hisi2 M06-ASO UF2NF1-Scripts
# @edt Curs 2017-2018  Febrer 2018
# -----------------------------------------------------------------

import sys,socket, time
from subprocess import Popen, PIPE

#Definició de variables i constants.
HOST = ''
PORT = 50005

# Conexió (sockets).
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST, PORT))
s.listen(1)

while True:

    try:
        conn, addr = s.accept()
    except Exception:
        continue

    print 'Connected by', addr

    timestamp = time.strftime("%Y-%m-%d-%H-%M-%s")
    filename = "%s-%s.txt" % (addr[0],timestamp)
    ff = open(filename, 'w')

    while True:
        data = conn.recv(1024)
        if not data:
            ff.close()
            break
        ff.write(data)

    conn.close()

sys.exit(0)
