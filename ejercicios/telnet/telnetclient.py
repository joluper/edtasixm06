#!/usr/bin/python 
#-*- coding: utf-8-*-
# exemple de echo server basic
# -----------------------------------------------------------------
# Escola del treball de Barcelona
# ASIX Hisi2 M06-ASO UF2NF1-Scripts
# @edt Curs 2017-2018  Febrer 2017
# -----------------------------------------------------------------
import sys,socket, argparse

#Gestió d'arguments amb argparse
parser = argparse.ArgumentParser(description=\
 """Programa que emula un client telnet""",
 prog="telnetserver.py")
parser.add_argument("-s","--server", dest="server", required=True, help="server")
parser.add_argument("-p","--port", dest="port", type=int, required=True, help="port")
args=parser.parse_args()

HOST = args.server
PORT = args.port
EOT = chr(4)

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))

while True:
    command = raw_input()
    if command == "quit": break
    s.send(command)
    while True:
        data = s.recv(1024)
        if data[-1] == EOT:
            print data[:-1]
            break
        print data
s.close()
sys.exit(0)
