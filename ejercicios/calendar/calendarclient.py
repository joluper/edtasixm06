#!/usr/bin/python 
#-*- coding: utf-8-*-
# exemple de echo server basic
# -----------------------------------------------------------------
# Escola del treball de Barcelona
# ASIX Hisi2 M06-ASO UF2NF1-Scripts
# @edt Curs 2017-2018  Febrer 2017
# -----------------------------------------------------------------
import sys,socket, argparse

#Gestió d'arguments amb argparse
parser = argparse.ArgumentParser(description=\
 """Programa que mostra un calendari donat
    un any en concret.""",\
 prog="calendarserver.py")
parser.add_argument("-s","--server", dest="server", required=True, help="server")
parser.add_argument("-p","--port", dest="port", type=int, required=True, help="port")
parser.add_argument("-y","--year", dest="year", required=True, help="year")
args=parser.parse_args()

HOST = args.server
PORT = args.port
YEAR = args.year

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))
s.send(YEAR)
while True:
    data = s.recv(1024)
    print data
    if not data: break
s.close()
sys.exit(0)
